﻿#PAiP Open License | English

### §0
This license is "PAiP Open License".
Created in 8 May 2016.
Text of that license is created in 10 May 2016.
More information on that address: [http://dev.paip.com.pl/license/paip-open-license/](http://dev.paip.com.pl/license/paip-open-license/)
Author of that license is Patryk Adamczyk i Przyjaciele Team.
Author of that license reserve the right to change text of license.

### §1
Software under that license referred to as Software.
Author or authors of software under that license referred to as Creator.

### §2
Software is free.

### §3
Anyone can copy it, modify, publish, use, compile, sell or distribute in any form, source code or compiled binary, to any use, commercial or non-commercial, by any means.

### §4
The Creator dedicate any and all copyright interest in the software to the public domain for the benefit of the public at large.

### §5
Software is provided "as is", without warranty of any kind. In no event shall the Creator be liable for any claim, damages or other liability, out of or in connection with the software or the use or other dealing in the software.

### §6
The Creator is absolutely committed to making the entire source code of the Software.
The Creator is obliged to publish the source code of every module of Software.
If the author has used third-party software is required to add a description to the source code with all the links to their ability to buy or download third-party software.