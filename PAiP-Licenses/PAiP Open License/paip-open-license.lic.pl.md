﻿#PAiP Open License | Polski

### §0
Ta licencja to "PAiP Open License".
Stworzona dnia 8 Maja 2016 roku.
Tekst tej licencji został stworzony dnia 10 Maja 2016 roku.
Więcej Informacji pod adresem : [http://dev.paip.com.pl/license/paip-open-license/](http://dev.paip.com.pl/license/paip-open-license/)
Autorem licencji jest zespół Patryk Adamczyk i Przyjaciele.
Autor Licencji zastrzega sobie prawo do zmiany tekstu licencji.

### §1
Oprogramowanie na tej licencji dalej zwane Oprogramowaniem.
Autor lub autorzy oprogramowania na tej licencji zwany dalej Twórcą.

### §2
Oprogramowanie jest darmowe i wolne.

### §3
Każdy może kopiować, modyfikować, publikować, używać, kompilować, sprzedawać lub rozpowszechniać w każdej formie czy kodu źródłowego czy skompilowanej, do każdego użytku, komercyjnego lub niekomercyjnego, i w dowolny sposób.

### §4
Twórca poświęca swoje prawa autorskie na rzecz domeny publicznej dla dobra ogółu społeczności korzystającej z oprogramowania.

### §5
Oprogramowanie jest dostarczane "takie jakie jest", bez jakiejkolwiek gwarancji. W żadnym wypadku Twórca nie będzie odpowiadać za jakiekolwiek roszczenia, szkody lub innych odpowiedzialności z lub związku z oprogramowaniem, lub jego użyciem, lub innej działalności z oprogramowaniem.