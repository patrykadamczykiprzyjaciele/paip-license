﻿#PAiP Client Project License | Polski
##Wersja 0.1 | 10.05.2016
##@Praca nad licencją skończona tymczasowo dnia 10 Maja 2016 o godzinie 20:56

##Sekcja 0 : Informacje o licencji
### §0
Ta licencja to "PAiP Client Project License".

Stworzona dnia 10 Maja 2016 roku.

Tekst tej licencji został stworzony dnia 10 Maja 2016 roku.

Więcej Informacji pod adresem : [http://dev.paip.com.pl/license/paip-client-project-license/](http://dev.paip.com.pl/license/paip-client-project-license/)

Autorem licencji jest zespół Patryk Adamczyk i Przyjaciele.

Autor Licencji zastrzega sobie prawo do zmiany tekstu licencji w każdej chwili.

Zmiany tej licencji wchodzą w życie 10 minut po publikacji nowej wersji licencji.

Zmiany licencji są stosowane do wszystkiego na niej do 2 lat do tyłu.

##Sekcja 1 : Terminologia Licencji
### §1
Oprogramowanie na tej licencji jest dalej zwane Oprogramowaniem.

Autor lub autorzy oprogramowania na tej licencji są zwani dalej Twórcą.

Wszyscy klienci korzystający z oprogramowania na tej licencji są zwani dalej Klientem.

##Sekcja 2 : Założenia Licencji
### §2
Oprogramowanie jest darmowe lub płatne. 

Informacje na ten temat patrz do Sekcji | Sekcja I : Informacje o oprogramowaniu |

### §3
Za Oprogramowanie długoterminowe musi zostać pobrana zaliczka w wysokości: 
0. 1 zł dla projektów niskich cen (1zł - 10zł)
0. 10 zł dla projektów średnich cen (10zł - 50zł)
0. 20% ceny początkowej (podawanej bezpośrednio po przyjęciu zamówienia).

Zaliczka jest wymagana do rozpoczęcia budowy Oprogramowania.

### §4
Twórca jest zobowiązany do trzymania terminu wykonania danego Oprogramowania.

### §5
Twórca jest zobowiązany do prezentacji lub przekazania Klientowi możliwości śledzenia postępów zamówienia.

### §6
Jeżeli klient zrezygnuje z zamówienia wtedy zapłacona zaliczka przepada.

### §7
Klient dostaje 1 dzień gwarancji działania.

Gwarancja zostaje zerwana gdy:
0. Minie okres gwarancyjny
0. Klient zmieni Oprogramowanie (np. Gdy klient dostanie gotową stronę i coś w niej zmieni)
0. Twórca zdecyduje przerwanie gwarancji podając powód

### §8
Jeżeli Klient znajdzie jakiś błąd w Oprogramowaniu w okresie gwarancji. Twórca jest zobowiązany do naprawy tego błędu.

Jeżeli Twórca nie zgodzi się na naprawę tego błędu.Twórca jest zobowiązany do wypłacenia rekompensaty w wysokości odpowiadającej procentom z ceny końcowej w wysokości ważności elementu będącego błędem. Ważność tą ustala Twórca.

### §9
Twórca zastrzega sobie prawo do rezygnacji z zamówienia i zwrotu całej zaliczki.

### §10


### §11


### §12


### §13


### §14


### §15


### §16


### §17


### §18


### §19


### §20

