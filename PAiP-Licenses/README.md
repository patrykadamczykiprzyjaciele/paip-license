#Aktualne Licencje
1. PAiP Open License                        | Wersja PL/EN | Wersja Licencji 0.1
2. PAiP Open Source License                 | Wersja PL/EN | Wersja Licencji 0.1

#Licencje które będą tworzone
1. PAiP Open Copyrighted License
2. PAiP Customer License
3. PAiP Web License
4. PAiP Free License
5. PAiP Commercial License
6. PAiP Non-Commercial License
7. PAiP Sell License